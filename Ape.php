<?php
    require_once('animal.php');
    class Ape extends animal{
        public $name;
        public $legs = 2;
        public $cold_blooded = "no";
        public $yell = "Auooo";

        public function __construct($string){
            $this->name = $string;
    }
    }
?>